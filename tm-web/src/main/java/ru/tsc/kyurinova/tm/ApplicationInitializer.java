package ru.tsc.kyurinova.tm;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.tsc.kyurinova.tm.config.ApplicationConfiguration;
import ru.tsc.kyurinova.tm.config.DataBaseConfiguration;
import ru.tsc.kyurinova.tm.config.WebApplicationConfiguration;
import ru.tsc.kyurinova.tm.config.WebConfig;

import javax.servlet.ServletContext;

/*
 * http://localhost:8080/ws/ProjectEndpoint.wsdl
 * http://localhost:8080/ws/TaskEndpoint.wsdl
 */

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfiguration.class, DataBaseConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebApplicationConfiguration.class, WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected void registerContextLoaderListener(ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }

}
