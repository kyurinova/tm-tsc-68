package ru.tsc.kyurinova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kyurinova.tm.api.service.model.IProjectService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyFieldException;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exeption.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.repository.model.ProjectRepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectService implements IProjectService {

    @Nullable
    @Autowired
    private ProjectRepository repository;

    @Override
    @Transactional
    public @NotNull Project add(@NotNull Project model) throws Exception {
        return repository.save(model);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public List<Project> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public @Nullable Project findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<Project> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(@Nullable Project model) throws Exception {
        if (model == null) throw new ProjectNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Project");
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public @Nullable Project update(@Nullable Project model) throws Exception {
        if (model == null) throw new ProjectNotFoundException();
        return repository.save(model);
    }

    @Override
    @Transactional
    public void changeProjectStatusById(@Nullable String id, @Nullable Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Project");
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @Override
    @Transactional
    public @NotNull Project create(@Nullable String name) throws Exception {
        if (name == null) throw new EmptyFieldException("Name");
        @Nullable final Project project = new Project(name);
        return repository.save(project);
    }

    @Override
    @Transactional
    public @NotNull Project create(@Nullable String name, @Nullable String description) throws Exception {
        if (name == null) throw new EmptyFieldException("Name");
        if (description == null) throw new EmptyFieldException("Description");
        @Nullable final Project project = new Project(name, description);
        return repository.save(project);
    }

    @Override
    @Transactional
    public void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Project");
        if (name == null) throw new EmptyFieldException("Name");
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
    }

}
