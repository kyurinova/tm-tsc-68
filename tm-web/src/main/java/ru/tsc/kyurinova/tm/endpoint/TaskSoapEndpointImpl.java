package ru.tsc.kyurinova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.kyurinova.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kyurinova.tm.dto.soap.*;

@Endpoint
public class TaskSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://kyurinova.tsc.ru/tm/dto/soap";

    @Autowired
    private ITaskDTOService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) throws Exception {
        return new TaskFindAllResponse(taskService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllByProjectIdRequest", namespace = NAMESPACE)
    public TaskFindAllByProjectIdResponse findAllByProjectId(@RequestPayload final TaskFindAllByProjectIdRequest request) throws Exception {
        return new TaskFindAllByProjectIdResponse(taskService.findAllByProjectId(request.getProjectId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskAddRequest", namespace = NAMESPACE)
    public TaskAddResponse add(@RequestPayload final TaskAddRequest request) throws Exception {
        return new TaskAddResponse(taskService.add(request.getTask()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) throws Exception {
        return new TaskSaveResponse(taskService.update(request.getTask()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) throws Exception {
        return new TaskFindByIdResponse(taskService.findOneById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse existsById(@RequestPayload final TaskExistsByIdRequest request) throws Exception {
        return new TaskExistsByIdResponse(taskService.findOneById(request.getId()) != null);
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(@RequestPayload final TaskCountRequest request) throws Exception {
        return new TaskCountResponse(taskService.count());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) throws Exception {
        taskService.removeById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) throws Exception {
        taskService.remove(request.getTask());
        return new TaskDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse clear(@RequestPayload final TaskDeleteAllRequest request) throws Exception {
        taskService.clear();
        return new TaskDeleteAllResponse();
    }

}
