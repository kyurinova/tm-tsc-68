package ru.tsc.kyurinova.tm.exeption.entity;

import ru.tsc.kyurinova.tm.exeption.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}