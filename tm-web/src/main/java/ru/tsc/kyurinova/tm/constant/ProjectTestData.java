package ru.tsc.kyurinova.tm.constant;

import org.jetbrains.annotations.NotNull;

public class ProjectTestData {

    @NotNull
    public final static String PROJECT1_NAME = "Project 1";

    @NotNull
    public final static String PROJECT1_DESCRIPTION = "Project 1 Description";

    @NotNull
    public final static String PROJECT2_NAME = "Project 2";

    @NotNull
    public final static String PROJECT2_DESCRIPTION = "Project 2 Description";

    @NotNull
    public final static String PROJECT3_NAME = "Project 3";

    @NotNull
    public final static String PROJECT3_DESCRIPTION = "Project 3 Description";

}
