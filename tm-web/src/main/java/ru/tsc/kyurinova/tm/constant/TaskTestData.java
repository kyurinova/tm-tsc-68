package ru.tsc.kyurinova.tm.constant;

import org.jetbrains.annotations.NotNull;

public class TaskTestData {

    @NotNull
    public final static String TASK1_NAME = "Task 1";

    @NotNull
    public final static String TASK1_DESCRIPTION = "Task 1 Description";

    @NotNull
    public final static String TASK2_NAME = "Task 2";

    @NotNull
    public final static String TASK2_DESCRIPTION = "Task 2 Description";

    @NotNull
    public final static String TASK3_NAME = "Task 3";

    @NotNull
    public final static String TASK3_DESCRIPTION = "Task 3 Description";

}
