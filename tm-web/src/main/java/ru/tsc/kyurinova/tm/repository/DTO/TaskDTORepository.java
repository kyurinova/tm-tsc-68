package ru.tsc.kyurinova.tm.repository.DTO;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    @NotNull
    List<TaskDTO> findByProjectId(@NotNull final String projectId);

}
