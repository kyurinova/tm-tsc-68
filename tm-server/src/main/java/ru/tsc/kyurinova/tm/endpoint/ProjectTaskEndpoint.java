package ru.tsc.kyurinova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.kyurinova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @Override
    @WebMethod
    @SneakyThrows
    public void bindTaskById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        sessionService.validate(session);
        projectTaskService.bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void unbindTaskById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        sessionService.validate(session);
        projectTaskService.unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @NotNull
    @WebMethod
    @SneakyThrows
    public void removeAllTaskByProjectId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        sessionService.validate(session);
        projectTaskService.removeAllTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        sessionService.validate(session);
        projectTaskService.removeById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    List<TaskDTO> findAllTaskByProjectId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        sessionService.validate(session);
        return projectTaskService.findAllTaskByProjectId(session.getUserId(), projectId);
    }
}
