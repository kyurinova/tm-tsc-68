package ru.tsc.kyurinova.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:config.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['application.version']}")
    private String applicationVersion;

    @Value("#{environment['developer.name']}")
    private String developerName;

    @Value("#{environment['developer.email']}")
    private String developerEmail;

}
